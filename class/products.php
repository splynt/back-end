<?php
    class Product{

        // Connection
        private $conn;

        // Table
        private $db_table = "product";

        // Columns
        public $id;
        public $sku;
        public $name;
        public $price;
        public $productType;
        public $dvdSize;
        public $bookWeight;
        public $furnitureHeight;
        public $furnitureWidth;
        public $furnitureLength;
        public $created;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getProducts(){
            $sqlQuery = "SELECT id, sku, name, price, productType, dvdSize, bookWeight,furnitureHeight,furnitureWidth,furnitureLength, created FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createProduct(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        sku = :sku, 
                        name = :name, 
                        price = :price, 
                        productType = :productType, 
                        dvdSize = :dvdSize,
                        bookWeight = :bookWeight,
                        furnitureHeight = :furnitureHeight,
                        furnitureWidth = :furnitureWidth,
                        furnitureLength = :furnitureLength,
                        created = :created";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->sku=htmlspecialchars(strip_tags($this->sku));
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->price=htmlspecialchars(strip_tags($this->price));
            $this->productType=htmlspecialchars(strip_tags($this->productType));
            $this->dvdSize=htmlspecialchars(strip_tags($this->dvdSize));
            $this->bookWeight=htmlspecialchars(strip_tags($this->bookWeight));
            $this->furnitureHeight=htmlspecialchars(strip_tags($this->furnitureHeight));
            $this->furnitureWidth=htmlspecialchars(strip_tags($this->furnitureWidth));
            $this->furnitureLength=htmlspecialchars(strip_tags($this->furnitureLength));
            $this->created=htmlspecialchars(strip_tags($this->created));
        
            // bind data
            $stmt->bindParam(":sku", $this->sku);
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":price", $this->price);
            $stmt->bindParam(":productType", $this->productType);
            $stmt->bindParam(":dvdSize", $this->dvdSize);
            $stmt->bindParam(":bookWeight", $this->bookWeight);
            $stmt->bindParam(":furnitureHeight", $this->furnitureHeight);
            $stmt->bindParam(":furnitureWidth", $this->furnitureWidth);
            $stmt->bindParam(":furnitureLength", $this->furnitureLength);
            $stmt->bindParam(":created", $this->created);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // READ single
        public function getSingleProduct(){
            $sqlQuery = "SELECT
                        id, 
                        sku,
                        name,
                        price,
                        productType,
                        dvdSize,
                        bookWeight,
                        furnitureHeight,
                        furnitureWidth,
                        furnitureLength,
                        created
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->sku = $dataRow['sku'];
            $this->name = $dataRow['name'];
            $this->price = $dataRow['price'];
            $this->productType = $dataRow['productType'];
            $this->dvdSize = $dataRow['dvdSize'];
            $this->bookWeight = $dataRow['bookWeight'];
            $this->furnitureHeight = $dataRow['furnitureHeight'];
            $this->furnitureWidth = $dataRow['furnitureWidth'];
            $this->furnitureLength = $dataRow['furnitureLength'];
            $this->created = $dataRow['created'];
        }        

        // UPDATE
        public function updateProduct(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        sku = :sku, 
                        name = :name,
                        price = :price,
                        productType = :productType,
                        dvdSize = :dvdSize,
                        bookWeight = :bookWeight,
                        furnitureHeight = :furnitureHeight,
                        furnitureWidth = :furnitureWidth,
                        furnitureLength = :furnitureLength,
                        created = :created
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->sku=htmlspecialchars(strip_tags($this->sku));
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->price=htmlspecialchars(strip_tags($this->price));
            $this->productType=htmlspecialchars(strip_tags($this->productType));
            $this->dvdSize=htmlspecialchars(strip_tags($this->dvdSize));
            $this->bookWeight=htmlspecialchars(strip_tags($this->bookWeight));
            $this->furnitureHeight=htmlspecialchars(strip_tags($this->furnitureHeight));
            $this->furnitureWidth=htmlspecialchars(strip_tags($this->furnitureWidth));
            $this->furnitureLength=htmlspecialchars(strip_tags($this->furnitureLength));
            $this->created=htmlspecialchars(strip_tags($this->created));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":sku", $this->sku);
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":price", $this->price);
            $stmt->bindParam(":productType", $this->productType);
            $stmt->bindParam(":dvdSize", $this->dvdSize);
            $stmt->bindParam(":bookWeight", $this->bookWeight);
            $stmt->bindParam(":furnitureHeight", $this->furnitureHeight);
            $stmt->bindParam(":furnitureWidth", $this->furnitureWidth);
            $stmt->bindParam(":furnitureLength", $this->furnitureLength);
            $stmt->bindParam(":created", $this->created);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteProduct(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
            
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>