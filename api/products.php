<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once '../config/database.php';
    include_once '../class/products.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Product($db);

    $stmt = $items->getProducts();
    $itemCount = $stmt->rowCount();


    if($itemCount > 0){
        
        $productArr = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "sku" => $sku,
                "name" => $name,
                "price" => $price,
                "productType" => $productType,
                "dvdSize" => $dvdSize,
                "bookWeight" => $bookWeight,
                "furnitureHeight" => $furnitureHeight,
                "furnitureWidth" => $furnitureWidth,
                "furnitureLength" => $furnitureLength,
                "created" => $created
            );

            array_push($productArr, $e);
        }
        echo json_encode($productArr);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("error" => "No record found.")
        );
    }
?>