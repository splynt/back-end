<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    
    include_once '../config/database.php';
    include_once '../class/products.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Product($db);
    
    $data = json_decode(file_get_contents("php://input"));
    
    $item->id = $data->id;
    
    if($item->deleteProduct()){
        
        $stmt = $item->getProducts();    
        $productArr = array();

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "sku" => $sku,
                "name" => $name,
                "price" => $price,
                "productType" => $productType,
                "dvdSize" => $dvdSize,
                "bookWeight" => $bookWeight,
                "furnitureHeight" => $furnitureHeight,
                "furnitureWidth" => $furnitureWidth,
                "furnitureLength" => $furnitureLength,
                "created" => $created
            );

            array_push($productArr, $e);
        }
        echo json_encode($productArr);
    } else{
        echo json_encode(
            array("error" => "Product could not be deleted")
        );
    }
?>