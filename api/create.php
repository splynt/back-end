<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/database.php';
    include_once '../class/products.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Product($db);

    $data = json_decode(file_get_contents("php://input"));


    $item->sku = $data->sku;
    $item->name = $data->name;
    $item->price = $data->price;
    $item->productType = $data->productType;
    $item->dvdSize = $data->dvdSize;
    $item->bookWeight = $data->bookWeight;
    $item->furnitureHeight = $data->furnitureHeight;
    $item->furnitureWidth = $data->furnitureWidth;
    $item->furnitureLength = $data->furnitureLength;
    $item->created = date('Y-m-d H:i:s');
    
    if(!empty($item->name)){
        if($item->createProduct()){
            echo json_encode(
                array('success'=> "Product created successfully")
            );
        } else{
            echo json_encode(
                array("error" => "Product could not be created.")
            );
        }   
    }
    else{
        echo json_encode(
            array("error" => "Product could not be created.")
        );
    }

?>